#!/bin/bash

sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf



cd /home/ec2-user/wordpress

## Checks if the namespace with the new tag already exists, if so it exits as a failure
if kubectl get namespace | grep -w wordpress-$NEW_WP
then
    echo "$NEW_WP already exists, please choose another"
    exit 1 
else
    ## Creates a new K8 yml file from a template yml file which contains the necessary resources for the wordpress blog
    cp K8_extra_users/k8-wordpress-rds-extra.yml K8_extra_users/k8-wordpress-rds-$NEW_WP.yml 
    sed -i "s,REPLACE,$NEW_WP," K8_extra_users/k8-wordpress-rds-$NEW_WP.yml
    cat K8_extra_users/k8-wordpress-rds-$NEW_WP.yml 

    sudo yum install -y mariadb
    ## Connects with the WordPress RDS database to create a database to accomodate the data of the new blog
    mysql -h ratstar-db-wp.academy.labs.automationlogic.com -u wordpress -pwordpress --execute="CREATE DATABASE IF NOT EXISTS wordpress_$NEW_WP;"

    kubectl apply -f K8_extra_users/k8-wordpress-rds-$NEW_WP.yml 
    exit 0
fi

