/* -------------------------------------------------------------------------- */
/*                   Store tfstate file in Private S3 Bucket                  */
/* -------------------------------------------------------------------------- */

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "ratstar-bucket"
    key            = "global/s3/terraform.tfstate"
    region         = "eu-west-1"
    
  }
}


module "jenkins" {
    source = "./modules/jenkins"

    vpc_id = var.vpc_id
    environment_tag = var.environment_tag
}

module "kubernetes" {
   source = "./modules/kubernetes"

   vpc_id = var.vpc_id
   environment_tag = var.environment_tag
   sg_jenkins_master = module.jenkins.sg_jenkins_master
   sg_jenkins_worker = module.jenkins.sg_jenkins_worker
   ssh_key = module.jenkins.ssh_key
   dbuser = module.db.dbuser
   dbpass = module.db.dbpass
   dbhost = module.db.dbhost
}

module "db" {
   source = "./modules/db"

   vpc_id = var.vpc_id
   environment_tag = var.environment_tag
   sg_jenkins_master = module.jenkins.sg_jenkins_master
   sg_jenkins_worker = module.jenkins.sg_jenkins_worker
   sg_kubernetes = module.kubernetes.sg_kubernetes
    
}

module "loadbalancer" {
   source = "./modules/loadbalancer"

   vpc_id = var.vpc_id
   environment_tag = var.environment_tag
   kubernetes_master0_id = module.kubernetes.kubernetes_master0_id
   kubernetes_master1_id = module.kubernetes.kubernetes_master1_id
    
}

module "s3" {
    source = "./modules/s3"

    environment_tag = var.environment_tag
    dbuser = module.db.dbuser
    dbpass = module.db.dbpass
    dbhost = module.db.dbhost
    
}

module "db_restore" {
   source = "./modules/db_restore"

   vpc_id = var.vpc_id
   environment_tag = var.environment_tag
   sg_jenkins_master = module.jenkins.sg_jenkins_master
   sg_jenkins_worker = module.jenkins.sg_jenkins_worker
   sg_kubernetes = module.kubernetes.sg_kubernetes
   sg_db = module.db.sg_db
   db_subnet_group = module.db.db_subnet_group
   db_id = module.db.db_id
    
}

