pipeline {
    agent {label 'Worker1'}
    
    stages {
        
        stage('Cloning Git') {
            steps {
                cleanWs()
                checkout([$class: 'GitSCM', branches: [[name: '*/prod']], extensions: [], userRemoteConfigs: [[credentialsId: '82d7f4ca-fd71-406c-adf8-92a36ba0f9f2', url: 'https://bitbucket.org/Tanya-Moller/assessment4/']]])       
            }
        }
        
        // Terraform Build
        stage('Building S3 Bucket') {
          steps{
            sh '''
            git config user.name 'Tanya'
            git config user.email 'tanya@automationlogic.com'
            git checkout prod
            cd terraform
            terraform init
            terraform apply -auto-approve -target=module.s3
            cd ..
            '''
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh("git add .")
            sh('git commit -m "{tf} tfstate files for S3 bucket Terraform build"')
            sh("git pull && git push git@bitbucket.org:Tanya-Moller/assessment4.git")
            }
            }
          }
          
          // Terraform Build
        stage('Building Kubernetes Cluster') {
          steps{
            sh '''
            git checkout prod
            cd terraform
            terraform init
            terraform apply -auto-approve -target=module.kubernetes
            cd ..
            '''
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh("git add .")
            sh('git commit -m "{tf} tfstate files for Kubernetes cluster Terraform build & provision"')
            sh("git pull && git push git@bitbucket.org:Tanya-Moller/assessment4.git")
            }
           }
          }
          
            // Terraform Build
        stage('Building RDS Database') {
          steps{
            sh '''
            git checkout prod
            cd terraform
            terraform init
            terraform apply -auto-approve -target=module.db
            cd ..
            '''
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh("git add .")
            sh('git commit -m "{tf} tfstate files for RDS Terraform build"')
            sh("git pull && git push git@bitbucket.org:Tanya-Moller/assessment4.git")
            }            
           }
          }
          
            // Terraform Build
        stage('Building Loadbalancer') {
          steps{
            sh '''
            git checkout prod
            cd terraform
            terraform init
            terraform apply -auto-approve -target=module.loadbalancer
            cd ..
            '''
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh("git add .")
            sh('git commit -m "{tf} tfstate files for Loadbalancer Terraform build"')
            sh("git pull && git push git@bitbucket.org:Tanya-Moller/assessment4.git")
            }
           }
          }
            // Sleep
        stage ("Wait prior to setting up K8") {
            steps{
                sh '''
                echo 'Waiting 3 minutes for deployment to complete prior to provisioning K8'
                sleep 180
                '''
                }
        }
        
        // K8 Setup
        stage('Provisioning K8 Nodes (SSH)') {
          steps{
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh '''
            master0_ip=`cat /home/ec2-user/master0_ip`
            master1_ip=`cat /home/ec2-user/master1_ip`
            node0_ip=`cat /home/ec2-user/node0_ip`
            node1_ip=`cat /home/ec2-user/node1_ip`
            node2_ip=`cat /home/ec2-user/node2_ip`
    
            ./pubkey.sh $master0_ip
            ./pubkey.sh $master1_ip
            ./pubkey.sh $node0_ip
            ./pubkey.sh $node1_ip
            ./pubkey.sh $node2_ip
            '''
            }
           }
          }
        
        // K8 Setup
        stage('Provisioning K8 Cluster') {
          steps{
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh '''
            controller_ip=`cat /home/ec2-user/controller_ip`
            
            ssh -o StrictHostKeyChecking=no ec2-user@$controller_ip '
            cd /home/ec2-user/kubespray
            ansible-playbook --ssh-common-args "-o StrictHostKeyChecking=no" -i /home/ec2-user/kubespray/inventory/mycluster/hosts.yaml --become --become-user=root /home/ec2-user/kubespray/cluster.yml
            '
            '''
            }
           }
          }
        
        // Terraform Build
        stage('Destroy K8 Controller') {
          steps{
            sh '''
            git checkout prod
            cd terraform
            terraform init
            terraform destroy -auto-approve -target=module.kubernetes.aws_instance.kubernetes_controller
            cd ..
            '''
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh("git add .")
            sh('git commit -m "{tf} Terraform controller destroyed"')
            sh("git pull && git push git@bitbucket.org:Tanya-Moller/assessment4.git")
            }
           }
          }
      
            // K8 Setup
        stage('Building K8 Dashboard') {
          steps{
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh '''
            master1_ip=`cat /home/ec2-user/master1_ip`

            scp k8-dashboard/haproxy-ingress.yml ec2-user@$master1_ip:/home/ec2-user/haproxy-ingress.yml
            scp k8-dashboard/kubernetes-dashboard-deployment.yml ec2-user@$master1_ip:/home/ec2-user/kubernetes-dashboard-deployment.yml

            ./k8-dashboard/k8_dashboard.sh $master1_ip
            '''
            }
           }
          }
      
      
      }
    }