#!/bin/bash

if (( $#  > 0 ))
then
    hostname=$1
else
    echo "WTF: you must supply a hostname or IP address" 1>&2 
    exit 1 
fi 

#Replace the Key with your key
ssh -o StrictHostKeyChecking=no -i ~/.ssh/ratstar-key.pem ec2-user@$hostname '

#Install git

sudo yum install git -y
yes | git clone git@bitbucket.org:Tanya-Moller/assessment4.git


#Install Terraform

sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform

cd assessment4
git checkout prod

cd terraform

terraform init
terraform apply -auto-approve -target=module.jenkins

'

## Need to add IAM role to instance for credentials