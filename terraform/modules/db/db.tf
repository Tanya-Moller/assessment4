/* -------------------------------------------------------------------------- */
/*                          Building DB for PetClinic                         */
/* -------------------------------------------------------------------------- */


/* -------------------------------------------------------------------------- */
/*                                 Database SG                                */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_db" {
  name = "sg_${var.environment_tag}_database"
  vpc_id = var.vpc_id
  description = "Allow 3306 from kubernetes and SSH from Jenkins"
  ingress {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      security_groups = [var.sg_kubernetes, var.sg_jenkins_worker]
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = [var.sg_jenkins_master, var.sg_jenkins_worker]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-database"
  }
}

/* -------------------------------------------------------------------------- */
/*                               DB Subnet Group                              */
/* -------------------------------------------------------------------------- */

resource "aws_db_subnet_group" "default" {
  name = "${var.environment_tag}-db-subnet-group"
  subnet_ids = ["subnet-a94474e1", "subnet-953a58cf",  "subnet-7bddfc1d"]
}

/* -------------------------------------------------------------------------- */
/*                      RDS Instance (Primary) (PetClinic)                    */
/* -------------------------------------------------------------------------- */

resource "aws_db_instance" "default" {
  allocated_storage = 20
  storage_type = "gp2"
  multi_az = true
  engine = "mysql"
  engine_version = "5.7.26"
  instance_class = "db.t2.micro"
  name = "petclinic"
  username = "petclinic"
  password = "petclinic"
  skip_final_snapshot = true
  vpc_security_group_ids = [aws_security_group.sg_db.id]
  db_subnet_group_name = aws_db_subnet_group.default.name
  identifier = "${var.environment_tag}-db"
  backup_retention_period = 5
  apply_immediately = true
  backup_window = "21:00-22:00"

  tags = {
      Name = "${var.environment_tag}_DB"
  }
}

/* -------------------------------------------------------------------------- */
/*                           RDS Instance (Replica)                           */
/* -------------------------------------------------------------------------- */

resource "aws_db_instance" "replica" {
  allocated_storage = 20
  storage_type = "gp2"
  multi_az = true
  engine = "mysql"
  engine_version = "5.7.26"
  instance_class = "db.t2.micro"
  skip_final_snapshot = true
  vpc_security_group_ids = [aws_security_group.sg_db.id]
  identifier = "${var.environment_tag}-db-replica"

  replicate_source_db = "${aws_db_instance.default.identifier}"
}


/* -------------------------------------------------------------------------- */
/*                     RDS Instance (Primary) (Wordpress)                     */
/* -------------------------------------------------------------------------- */

resource "aws_db_instance" "wp" {
  allocated_storage = 20
  storage_type = "gp2"
  multi_az = true
  engine = "mysql"
  engine_version = "5.7.26"
  instance_class = "db.t2.micro"
  name = "wordpress"
  username = "wordpress"
  password = "wordpress"
  skip_final_snapshot = true
  vpc_security_group_ids = [aws_security_group.sg_db.id]
  db_subnet_group_name = aws_db_subnet_group.default.name
  identifier = "${var.environment_tag}-db-wp"
  backup_retention_period = 5
  apply_immediately = true
  backup_window = "21:00-22:00"

  tags = {
      Name = "${var.environment_tag}_DB_WP"
  }
}

/* -------------------------------------------------------------------------- */
/*                                    DNS Name                                */
/* -------------------------------------------------------------------------- */

resource "aws_route53_record" "database" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "ratstar-db.academy.labs.automationlogic.com"
  type = "CNAME"
  ttl = "300"
  records = [aws_db_instance.default.address]
}

resource "aws_route53_record" "database-wp" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "ratstar-db-wp.academy.labs.automationlogic.com"
  type = "CNAME"
  ttl = "300"
  records = [aws_db_instance.wp.address]
}
