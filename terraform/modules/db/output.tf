output "sg_db" {
    value = aws_security_group.sg_db.id
}

output "db_subnet_group" {
    value = aws_db_subnet_group.default.name
}

output "db_id" {
    value = aws_db_instance.default.id
}

output "dbuser" {
    value = aws_db_instance.default.username
}

output "dbpass" {
    value = aws_db_instance.default.password
}

output "dbhost" {
    value = aws_db_instance.default.address
}
