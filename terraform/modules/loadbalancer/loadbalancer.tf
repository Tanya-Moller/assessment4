
/* -------------------------------------------------------------------------- */
/*                         Loadbalancer Security Group                        */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_lb" {
  name = "sg_${var.environment_tag}_loadbalancer"
  vpc_id = var.vpc_id
  description = "Allow access from all and SSH from Jenkins"
  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }
ingress {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-loadbalancer"
  }
}


/* -------------------------------------------------------------------------- */
/*                                Load Balancer                               */
/* -------------------------------------------------------------------------- */

resource "aws_lb" "default" {
  name               = "${var.environment_tag}-loadbalancer"
  internal           = false
  load_balancer_type = "application"
  security_groups = [ aws_security_group.sg_lb.id ]
  subnets = ["subnet-a94474e1", "subnet-953a58cf",  "subnet-7bddfc1d"]

  tags = {
      Name = "${var.environment_tag}_lb"
    }
}

/* -------------------------------------------------------------------------- */
/*                                Target Group                                */
/* -------------------------------------------------------------------------- */

resource "aws_lb_target_group" "default" {
  name     = "${var.environment_tag}-lb-tg"
  port     = 31247
  protocol = "HTTP"
  target_type = "instance"

  vpc_id   = var.vpc_id
    health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    }
}


resource "aws_lb_target_group_attachment" "default" {
  target_group_arn = aws_lb_target_group.default.arn
  target_id = var.kubernetes_master0_id
}

resource "aws_lb_target_group_attachment" "default2" {
  target_group_arn = aws_lb_target_group.default.arn
  target_id = var.kubernetes_master1_id
}

resource "aws_lb_target_group" "k8dash" {
  name     = "${var.environment_tag}-k8-dash-tg"
  port     = 31106
  protocol = "HTTPS"
  target_type = "instance"

  vpc_id   = var.vpc_id
    health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    }
}

resource "aws_lb_target_group_attachment" "default3" {
  target_group_arn = aws_lb_target_group.k8dash.arn
  target_id = var.kubernetes_master0_id
}

resource "aws_lb_target_group_attachment" "default4" {
  target_group_arn = aws_lb_target_group.k8dash.arn
  target_id = var.kubernetes_master1_id
}


/* -------------------------------------------------------------------------- */
/*                         Listener for Load Balancer                         */
/* -------------------------------------------------------------------------- */

resource "aws_lb_listener" "default" {
  load_balancer_arn = aws_lb.default.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.default.arn
  }
}

resource "aws_lb_listener" "k8dash" {
  load_balancer_arn = aws_lb.default.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  certificate_arn   = "arn:aws:acm:eu-west-1:242392447408:certificate/17a12f0a-ba1e-44d9-a1b2-44aca0bb9f4f"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.k8dash.arn
  }
}


/* -------------------------------------------------------------------------- */
/*                             DNS Name (Route 53)                            */
/* -------------------------------------------------------------------------- */

resource "aws_route53_record" "lb" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.environment_tag}-petclinic.academy.labs.automationlogic.com"
  type    = "A"

  alias {
    name                   = "${aws_lb.default.dns_name}"
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "lbwp" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.environment_tag}-wordpress.academy.labs.automationlogic.com"
  type    = "A"

  alias {
    name                   = "${aws_lb.default.dns_name}"
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "kube" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.environment_tag}-kubedash.academy.labs.automationlogic.com"
  type    = "A"

  alias {
    name                   = "${aws_lb.default.dns_name}"
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ram" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.environment_tag}-wordpress-ramana.academy.labs.automationlogic.com"
  type    = "A"

  alias {
    name                   = "${aws_lb.default.dns_name}"
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "tan" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.environment_tag}-wordpress-tanya.academy.labs.automationlogic.com"
  type    = "A"

  alias {
    name                   = "${aws_lb.default.dns_name}"
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "nginx" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.environment_tag}-web.academy.labs.automationlogic.com"
  type    = "A"

  alias {
    name                   = "${aws_lb.default.dns_name}"
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "pc-dev" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.environment_tag}-petclinic-dev.academy.labs.automationlogic.com"
  type    = "A"

  alias {
    name                   = "${aws_lb.default.dns_name}"
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}


