/* -------------------------------------------------------------------------- */
/*                          Kubernetes Security Group                         */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_kubernetes" {
  name = "sg_ratstar_kubernetes_master"
  vpc_id = var.vpc_id
  description = "Allow access from home Jenkins"
  ingress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      security_groups = [var.sg_jenkins_worker]
  }
  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      self = true
      description = "allows ssh from self"
    }
  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["172.31.0.0/16"]
      description = "allows ssh from VPC"
    }
ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
       cidr_blocks = ["185.59.124.94/32", "90.199.173.250/32"]
      description = "allows ssh from ramtan"
    }


    
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-kubernetes_master"
  }
}

/* -------------------------------------------------------------------------- */
/*                    Kubernetes Master and Nodes Instances                   */
/* -------------------------------------------------------------------------- */
resource "aws_instance" "kubernetes_controller" {
  ami = "ami-063d4ab14480ac177"
  root_block_device {
        volume_size = 20
    }
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.sg_kubernetes.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = var.ssh_key
  user_data = base64encode(templatefile(".//modules/kubernetes/provision.tpl", 
  {
    dbuser = "${var.dbuser}"
    dbpass = "${var.dbpass}"
    dbhost = "${var.dbhost}"
    priv_ip_master1 = "${aws_instance.kubernetes_master[0].private_ip}"
    priv_ip_master2 = "${aws_instance.kubernetes_master[1].private_ip}"
    priv_ip_worker1 = "${aws_instance.kubernetes_node[0].private_ip}"
    priv_ip_worker2 = "${aws_instance.kubernetes_node[1].private_ip}"
    priv_ip_worker3 = "${aws_instance.kubernetes_node[2].private_ip}"
  }
  
  ))
  tags = {
      "Name" = "${var.environment_tag}-kubernetes-controller"
  }
}

resource "aws_instance" "kubernetes_master" {
  ami = "ami-063d4ab14480ac177"
    root_block_device {
        volume_size = 30
    }
  count = 2
  instance_type = "t3a.xlarge"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.sg_kubernetes.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = var.ssh_key
  user_data = "${base64encode(<<-EOF
            #!/bin/bash

            #Install git
            sudo yum install git -y
            git clone https://Tanya-Moller@bitbucket.org/Tanya-Moller/assessment4.git /home/ec2-user/assessment4

            # Clone HAProxy repo
            git clone https://github.com/haproxytech/kubernetes-ingress.git /home/ec2-user/kubernetes-ingress

            #Install Docker & Docker Compose
            sudo yum install -y docker
            sudo systemctl start docker
            sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
            sudo chmod +x /usr/local/bin/docker-compose
            sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

            EOF
  )}"
  tags = {
      "Name" = "${var.environment_tag}-k8-master-${count.index}"
  }
}

resource "aws_instance" "kubernetes_node" {
  ami = "ami-063d4ab14480ac177"
  count = 3
    root_block_device {
        volume_size = 30
    }
  instance_type = "t3a.xlarge"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.sg_kubernetes.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = var.ssh_key
  user_data = "${base64encode(<<-EOF
            #!/bin/bash

            #Install git
            sudo yum install git -y
            
            
            EOF
  )}"
  tags = {
      "Name" = "${var.environment_tag}-k8-node-${count.index}"
  }
}

/* -------------------------------------------------------------------------- */
/*                     Create Local Files with Private IPs                    */
/* -------------------------------------------------------------------------- */

resource "local_file" "controller" {
    content = aws_instance.kubernetes_controller.private_ip
    filename = "/home/ec2-user/controller_ip"
}

resource "local_file" "master0" {
    content = aws_instance.kubernetes_master[0].private_ip
    filename = "/home/ec2-user/master0_ip"
}

resource "local_file" "master1" {
    content = aws_instance.kubernetes_master[1].private_ip
    filename = "/home/ec2-user/master1_ip"
}

resource "local_file" "node0" {
    content = aws_instance.kubernetes_node[0].private_ip
    filename = "/home/ec2-user/node0_ip"
}

resource "local_file" "node1" {
    content = aws_instance.kubernetes_node[1].private_ip
    filename = "/home/ec2-user/node1_ip"
}

resource "local_file" "node2" {
    content = aws_instance.kubernetes_node[2].private_ip
    filename = "/home/ec2-user/node2_ip"
}



