output "sg_kubernetes" {
    description = "kubernetes security group"
    value = aws_security_group.sg_kubernetes.id
}

output "kubernetes_master0_id" {
    value = aws_instance.kubernetes_master[0].id
}

output "kubernetes_master1_id" {
    value = aws_instance.kubernetes_master[1].id
}
