#!/bin/bash
sudo yum update -y

# ---------------------------------------------------------------------------- #
#                        Install Kubespray requirements                        #
# ---------------------------------------------------------------------------- #
sudo yum install -y git
yes | git clone https://github.com/kubernetes-sigs/kubespray.git /home/ec2-user/kubespray
sudo pip3 install -r /home/ec2-user/kubespray/requirements.txt
cp -rfp /home/ec2-user/kubespray/inventory/sample /home/ec2-user/kubespray/inventory/mycluster


# ---------------------------------------------------------------------------- #
#                                Edit hosts file                               #
# ---------------------------------------------------------------------------- #
echo "all:
  hosts:
    node1:
      ansible_host: ${priv_ip_master1}
      ip: ${priv_ip_master1}
      access_ip: ${priv_ip_master1}
    node2:
      ansible_host: ${priv_ip_master2}
      ip: ${priv_ip_master2}
      access_ip: ${priv_ip_master2}   
    node3:
      ansible_host: ${priv_ip_worker1}
      ip: ${priv_ip_worker1}
      access_ip: ${priv_ip_worker1}
    node4:
      ansible_host: ${priv_ip_worker2}
      ip: ${priv_ip_worker2}
      access_ip: ${priv_ip_worker2}
    node5:
      ansible_host: ${priv_ip_worker3}
      ip: ${priv_ip_worker3}
      access_ip: ${priv_ip_worker3}
  children:
    kube_control_plane:
      hosts:
        node1:
        node2:
    kube_node:
      hosts:
        node3:
        node4:
        node5:
    etcd:
      hosts:
        node1:
        node2:
        node3:
        node4:
        node5:
    k8s_cluster:
      children:
        kube_control_plane:
        kube_node:
    calico_rr:
      hosts: {}
">/home/ec2-user/kubespray/inventory/mycluster/hosts.yaml

# ---------------------------------------------------------------------------- #
#                     Generate SSH key for access to nodes                     #
# ---------------------------------------------------------------------------- #
ssh-keygen -b 2048 -t rsa -f /home/ec2-user/.ssh/sshkey -q -N ""

# ---------------------------------------------------------------------------- #
#                             Allow ec2-user access                            #
# ---------------------------------------------------------------------------- #
sudo chown ec2-user:ec2-user /home/ec2-user/.ssh/sshkey
sudo chown ec2-user:ec2-user /home/ec2-user/.ssh/sshkey.pub

# ---------------------------------------------------------------------------- #
#                             Allow ec2-user access                            #
# ---------------------------------------------------------------------------- #
sudo chown -R ec2-user:ec2-user /home/ec2-user/*

# ---------------------------------------------------------------------------- #
#              Edit ansible config file for path access to ssh key             #
# ---------------------------------------------------------------------------- #
sudo sh -c "cat >/home/ec2-user/kubespray/ansible.cfg <<_END_
[ssh_connection]
pipelining=True
ssh_args = -o ControlMaster=auto -o ControlPersist=30m -o ConnectionAttempts=100 -o UserKnownHostsFile=/dev/null
#control_path = ~/.ssh/ansible-%%r@%%h:%%p
[defaults]
strategy_plugins = plugins/mitogen/ansible_mitogen/plugins/strategy
# https://github.com/ansible/ansible/issues/56930 (to ignore group names with - and .)
force_valid_group_names = ignore
private_key_file = /home/ec2-user/.ssh/sshkey

host_key_checking = False
gathering = smart
fact_caching = jsonfile
fact_caching_connection = /tmp
fact_caching_timeout = 7200
stdout_callback = default
display_skipped_hosts = no
library = ./library
callback_whitelist = profile_tasks
roles_path = roles:$VIRTUAL_ENV/usr/local/share/kubespray/roles:$VIRTUAL_ENV/usr/local/share/ansible/roles:/usr/share/kubespray/roles
deprecation_warnings=False
inventory_ignore_extensions = ~, .orig, .bak, .ini, .cfg, .retry, .pyc, .pyo, .creds, .gpg
[inventory]
ignore_patterns = artifacts, credentials
_END_"

# ---------------------------------------------------------------------------- #
#                     Copy the public ssh key to s3 bucket                     #
# ---------------------------------------------------------------------------- #
aws s3 cp /home/ec2-user/.ssh/sshkey.pub s3://ratstar-bucket/sshkey.pub

# ---------------------------------------------------------------------------- #
#                     Clone the petclinic repo and change permissions                    #
# ---------------------------------------------------------------------------- #
git clone https://github.com/spring-projects/spring-petclinic.git /app
chmod +x /app/src/main/resources/db/mysql/user.sql
chmod +x /app/src/main/resources/db/mysql/schema.sql
chmod +x /app/src/main/resources/db/mysql/data.sql

# ---------------------------------------------------------------------------- #
#                    Install mariaDB and input the SQL files                   #
# ---------------------------------------------------------------------------- #
sudo yum -y install mariadb
mysql -u ${dbuser} -p${dbpass} -h ${dbhost} --execute="source /app/src/main/resources/db/mysql/user.sql;" 
mysql -u ${dbuser} -p${dbpass} -h ${dbhost} --execute="source /app/src/main/resources/db/mysql/schema.sql;" --database=petclinic
mysql -u ${dbuser} -p${dbpass} -h ${dbhost} --execute="source /app/src/main/resources/db/mysql/data.sql;" --database=petclinic

