variable "vpc_id" {

}

variable "environment_tag" {

}

variable "sg_jenkins_master" {

}

variable "sg_jenkins_worker" {
    
}

variable "ssh_key" {

}

variable "dbuser" {

}

variable "dbpass" {
    
}

variable "dbhost" {
    
}
