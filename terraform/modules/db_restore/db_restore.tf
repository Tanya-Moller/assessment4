/* -------------------------------------------------------------------------- */
/*                          Database Restore Instance                         */
/* -------------------------------------------------------------------------- */

resource "aws_db_instance" "restore" {
  allocated_storage = 20
  storage_type = "gp2"
  multi_az = true
  engine = "mysql"
  engine_version = "5.7.26"
  instance_class = "db.t2.micro"
  name = "petclinic"
  username = "petclinic"
  password = "petclinic"
  skip_final_snapshot = true
  snapshot_identifier = "rdssnap"

  lifecycle {
    ignore_changes = [snapshot_identifier]
  }
  vpc_security_group_ids = ["${var.sg_db}"]
  db_subnet_group_name = "${var.db_subnet_group}"
  identifier = "${var.environment_tag}-db-restore"
  backup_retention_period = 5
  apply_immediately = true
  backup_window = "21:00-22:00"

  tags = {
      Name = "${var.environment_tag}_DB"
  }
}