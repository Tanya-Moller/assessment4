variable "vpc_id" {

}

variable "environment_tag" {

}

variable "sg_kubernetes" {

}

variable "sg_jenkins_master" {

}

variable "sg_jenkins_worker" {
    
}

variable "sg_db" {

}

variable "db_subnet_group" {

}

variable "db_id" {
    
}