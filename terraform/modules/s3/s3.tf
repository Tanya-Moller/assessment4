/* -------------------------------------------------------------------------- */
/*                            Create AWS S3 Bucket                            */
/* -------------------------------------------------------------------------- */

resource "aws_s3_bucket" "default" {
  bucket = "ratstar-bucket"
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = {
    Name        = "ratstar-bucket"
  }
}

resource "aws_s3_bucket_object" "default" {
  bucket = "ratstar-bucket"
  key    = "pc_config.txt"
  content = "Username: ${var.dbuser}\nEndpoint: ${var.dbhost}\nPassword: ${var.dbpass}"
}

/* -------------------------------------------------------------------------- */
/*                 Create Policy to Allow Access From Home IPs                */
/* -------------------------------------------------------------------------- */

resource "aws_s3_bucket_policy" "default" {
  bucket = aws_s3_bucket.default.id

  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "S3PolicyId1"
    Statement = [
      {
        Sid       = "IPAllow"
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:*"
        Resource = [ aws_s3_bucket.default.arn,
          "${aws_s3_bucket.default.arn}/*"]
        Condition = {
          IpAddress = {
            "aws:SourceIp" = [
                        "185.59.124.94/32",
                        "87.81.71.242/32",
                        "152.37.90.101",
                        "87.80.216.64"
                    ]
          }
        }
      },
    ]
  })
}

