/* -------------------------------------------------------------------------- */
/*                   Jenkins Master & Worker Security Groups                  */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_jenkins_master" {
  name = "sg_ratstar_jenkins_master"
  vpc_id = var.vpc_id
  description = "Allow access from home IPs"
  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["87.81.71.242/32", "185.59.124.94/32", "87.80.216.64/32", "152.37.90.101/32", "90.199.173.250/32"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-jenkins_master"
  }
}

resource "aws_security_group" "sg_jenkins_worker" {
  name = "sg_ratstar_jenkins_worker"
  vpc_id = var.vpc_id
  description = "Allow access from home IPs and Jenkins IPs"
  ingress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      security_groups = [aws_security_group.sg_jenkins_master.id]
  }
    ingress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      self = true
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-jenkins_worker"
  }
}

/* -------------------------------------------------------------------------- */
/*                    Webhook (BitBucket IP) Security Group                   */
/* -------------------------------------------------------------------------- */

resource "aws_security_group" "sg_bitbucket" {
  name = "sg_ratstar_bitbucket"
  vpc_id = var.vpc_id
  description = "Allow access from BitBucket IPs"
  ingress {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["13.52.5.96/28", "13.236.8.224/28", "18.136.214.96/28", "18.184.99.224/28", "18.234.32.224/28", "18.246.31.224/28", "52.215.192.224/28", "104.192.137.240/28", "104.192.138.240/28", "104.192.140.240/28", "104.192.142.240/28", "104.192.143.240/28", "185.166.143.240/28", "185.166.142.240/28"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-bitbucket"
  }
}

/* -------------------------------------------------------------------------- */
/*                                   SSH Key                                  */
/* -------------------------------------------------------------------------- */

resource "aws_key_pair" "default" {
    key_name = "ratstar_key"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDK/g8DwDLifr6pTqqzm9tK0OFEbtKGsPRYI+9dZfDBP7Wvyd8xL/wPwpPwKN3QbKo69Q8CqgefSauzMRs1uf8rlz6cVSNRI4S270GOFtdmituCVdA61l1FqtcAx6feOk1iM2Hl3wXZcLualx9sJ7hpYvLorv29Km3cEbb+1Mk0HpAfmYXqsjxB/Iw0SfJ2urr33kSw29n04IrYyUD3NKs+c5hy2KaFZFRobzjSZ7xjzcGPfJVjIQtJifSIMTfKypZAiqm+73UHljuTvRM9/VaIt/JrVuTusNTPheypfJfdubXGznPlMcVrxbI9kgnXHt1rzwXtbwpZFqmmS9gqTx5"
}

/* -------------------------------------------------------------------------- */
/*                    Jenkins Master and 2 Worker instances                   */
/* -------------------------------------------------------------------------- */

resource "aws_instance" "jenkins_master" {
  ami = "ami-05efd4267bf857368" 
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
#   subnet_id = aws_subnet.subnet_public_1a.id
  vpc_security_group_ids = [aws_security_group.sg_jenkins_master.id, aws_security_group.sg_bitbucket.id]
  key_name = aws_key_pair.default.key_name
  user_data = "${base64encode(<<-EOF
            #!/bin/bash

            #Update yum
            sudo yum update -y

            #Install Apache and create reverse proxy
            sudo yum install -y httpd
            sudo systemctl start httpd
            cd /etc/httpd/conf.d
            sudo sh -c "echo \"<VirtualHost *:80>
            ProxyPreserveHost On

            ProxyPass / http://127.0.0.1:8080/
            ProxyPassReverse / http://127.0.0.1:8080/
            </VirtualHost>\" > jenkins.conf"
            sudo systemctl restart httpd
            sudo systemctl enable httpd

            EOF
  )}"
  tags = {
      "Name" = "${var.environment_tag}-jenkins-master"
  }
}

resource "aws_instance" "jenkins_worker" {
  ami = "ami-0ec3d4522263c57e6"
  root_block_device {
        volume_size = 20
    }
  count = 2
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
#   subnet_id = aws_subnet.subnet_public_1a.id
  vpc_security_group_ids = [aws_security_group.sg_jenkins_worker.id, aws_security_group.sg_bitbucket.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = aws_key_pair.default.key_name
  user_data = filebase64(".//modules/jenkins/provision.tpl")
  tags = {
      "Name" = "${var.environment_tag}-jenkins-worker-${count.index}"
  }
}

/* -------------------------------------------------------------------------- */
/*                                  DNS Name                                  */
/* -------------------------------------------------------------------------- */

resource "aws_route53_record" "jenkins" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.environment_tag}-jenkins.academy.labs.automationlogic.com"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.jenkins_master.public_ip]
}

resource "local_file" "worker1" {
    content = aws_instance.jenkins_worker[0].private_ip
    filename = "/home/ec2-user/jenk0_ip"
}

resource "local_file" "worker2" {
    content = aws_instance.jenkins_worker[1].private_ip
    filename = "/home/ec2-user/jenk1_ip"
}
