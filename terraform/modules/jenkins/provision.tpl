#!/bin/bash
sudo yum update -y

# ---------------------------------------------------------------------------- #
#                                  Install Git                                 #
# ---------------------------------------------------------------------------- #
sudo yum install -y git
git clone git@bitbucket.org:Tanya-Moller/assessment4.git

# ---------------------------------------------------------------------------- #
#                               Install Terraform                              #
# ---------------------------------------------------------------------------- #
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform

# ---------------------------------------------------------------------------- #
#                        Install Docker & Docker Compose                       #
# ---------------------------------------------------------------------------- #
sudo yum install -y docker
sudo systemctl start docker

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# ---------------------------------------------------------------------------- #
#                                Install Ansible                               #
# ---------------------------------------------------------------------------- #
sudo amazon-linux-extras install ansible2

