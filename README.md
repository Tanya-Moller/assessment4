# Kubernetes - Assessment 4

### Links to Working Applications
[Jenkins Server](http://ratstar-jenkins.academy.labs.automationlogic.com)

[Trello Board](https://trello.com/b/dmppwwdo/star-assessment4)

[Petclinic](http://ratstar-petclinic.academy.labs.automationlogic.com/)

[Wordpress](http://ratstar-wordpress.academy.labs.automationlogic.com/)

[Static Website](http://ratstar-web.academy.labs.automationlogic.com/)

[Docker hub](https://hub.docker.com/repository/docker/ratstardocker/petclinic)

[Kubernetes Dashboard](https://ratstar-kubedash.academy.labs.automationlogic.com/#/login) - Requires certificate (see Section 2.2)


---
##  Table of Contents

- [Kubernetes - Assessment 4](#kubernetes---assessment-4)
    - [Links to Working Applications](#links-to-working-applications)
  - [Table of Contents](#table-of-contents)
  - [- To Launch These Dev Changes To Production](#--to-launch-these-dev-changes-to-production)
- [1. Introduction](#1-introduction)
  - [1.1 Summary](#11-summary)
- [2. Infrastructure](#2-infrastructure)
  - [2.1 Terraform Jenkins Setup](#21-terraform-jenkins-setup)
    - [To run](#to-run)
  - [2.2 Terraform AWS Infrastructure and Kubernetes Cluster Setup](#22-terraform-aws-infrastructure-and-kubernetes-cluster-setup)
    - [Jenkins View  `1. Initialise`](#jenkins-view--1-initialise)
    - [Job A. `launch_aws_infrastructure_and_k8`](#job-a-launch_aws_infrastructure_and_k8)
      - [Stage 1. Cloning Git](#stage-1-cloning-git)
      - [Stage 2. Building S3 Bucket](#stage-2-building-s3-bucket)
      - [Stage 3. Building RDS Database](#stage-3-building-rds-database)
      - [Stage 4. Building Kubernetes Cluster](#stage-4-building-kubernetes-cluster)
      - [Stage 5. Building Loadbalancer](#stage-5-building-loadbalancer)
      - [Stage 6. Sleep](#stage-6-sleep)
      - [Stage 7. Provisioning K8 Nodes](#stage-7-provisioning-k8-nodes)
      - [Stage 8. Provisioning K8 Cluster](#stage-8-provisioning-k8-cluster)
      - [Stage 9. Destroy K8 Controller](#stage-9-destroy-k8-controller)
      - [Stage 10. Building K8 Dashboard](#stage-10-building-k8-dashboard)
      - [Stage 11. Slack notification](#stage-11-slack-notification)
- [3. Docker Registry](#3-docker-registry)
    - [Docker Images](#docker-images)
- [4. Deployment](#4-deployment)
  - [4.1 PetClinic](#41-petclinic)
    - [Deployment](#deployment)
      - [**Stage 10. Building K8 Dashboard**](#stage-10-building-k8-dashboard-1)
      - [**Stage 11. Slack notification**](#stage-11-slack-notification-1)
- [<a name='Docker Registry'></a>3. Docker Registry](#3-docker-registry-1)
    - [Docker Images](#docker-images-1)
        - [Metric Server](#metric-server)
    - [Service](#service)
    - [Ingress](#ingress)
    - [Ingress Controller](#ingress-controller)
  - [4.2 WordPress](#42-wordpress)
    - [Description of Application Set Up](#description-of-application-set-up)
    - [Launching New Blogs](#launching-new-blogs)
      - [Detail of Jenkins Job](#detail-of-jenkins-job)
  - [4.3 Static Website](#43-static-website)
    - [Jenkins job](#jenkins-job)
    - [Static web deploy file](#static-web-deploy-file)
- [5. Management](#5-management)
  - [5.1 Generate Token for Kubernetes Dashboard](#51-generate-token-for-kubernetes-dashboard)
    - [Job `k8_dashboard_token_generator`](#job-k8_dashboard_token_generator)
  - [5.2 Change PetClinic RDS Password](#52-change-petclinic-rds-password)
    - [Job `password_change`](#job-password_change)
  - [5.3 Change PetClinic RDS Connection](#53-change-petclinic-rds-connection)
    - [Job `petclinic_update_db_connection`](#job-petclinic_update_db_connection)
  - [5.4 Restore Database](#54-restore-database)
    - [Job `restore_database`](#job-restore_database)
- [6. Development Environment](#6-development-environment)
  - [Using the Dev Environment](#using-the-dev-environment)
  - [Launching A New PetClinic Dev Env](#launching-a-new-petclinic-dev-env)
  - [To Launch These Dev Changes To Production](#to-launch-these-dev-changes-to-production)
---
#  1. Introduction

This is a guide of how to launch a Kubernetes cluster, and deploy working PetClinic and Wordpress Application through a CI/CD pipeline on Jenkins.
***
## 1.1 Summary

1. Jenkins will run *launch_aws_infrastructure_and_k8*. which is a pipeline that will build the whole infrastructure (including RDS, instances, loadbalancer and S3 Bucket). Within this pipeline all the instances are also provisioned accordingly, so 2 kubernetes master nodes will be produces and 4 worker nodes and 1 controller. After this is provisioned, the controller is destroyed. Lastly, a slack notification is posted on the *academy api channel*.

2. The developer can make a change to the petclinic spring repository *https://bitbucket.org/JangleFett/petclinic/src/master/* and then run the jenkins job called *k8_deploy_petclinic_dev*. This will deploy these changes locally to the following DNS:
 ratstar-petclinic-dev.academy.labs.automationlogic.com
On this DNS developers can view the changes before deploying.

3. After approving these changes, the devloper would need to click on the jenkins job called *docker_build*. This will create a docker image and push to the repository, with the tag of the jenkins build number, and trigger the job called *k8_deploy_petclinic*.
   
4. The *k8_deploy_petclinic* job replaces the docker image version number with the jenkins build number and runs a file called deploy_pc_k8.sh. This file creates a kubernetes seceret for docker registry then deploys the petclninc.yml, haproxy_server.yaml and metrics_server.yml.

After this build is complete, changes can be seen on the following DNS:
   ratstar-petclinic.academy.labs.automationlogic.com


![](images/structure.png)


# 2. Infrastructure
## 2.1 Terraform Jenkins Setup 
***
- The module `jenkins` is used for this Terraform build.
- The Terraform file can be found at `terraform/modules/jenkins/jenkins.tf` in the repo.
- Security groups are created for the master and worker nodes, as well as the BitBucket IPs to allow for webhook connection.
- An AWS keypair is created.
- A master node instance is created and provisoned with a reverse proxy to allow access on port 80.
  - AMIs have been used with Jenkins installed and set up.
- 2 worker nodes instances are created with the necessary IAM roles and are provisioned with the file `terraform/modules/jenkins/provision.tpl` in the repo.
  - The comment notes in the file explain the provisioning with the necessary packages.

### To run
- Use an instance with an IAM role that allows the necessary access in AWS to create the resources.
- Ensure Terraform is installed.
- The script `initialize.sh` is used to create this.
- In local machine run the following command to set up Jenkins.
- Ensure you are in the repo directory.
- Sign into the Jenkins GUI with the link and update the worker nodes with their new private IPs. 
  - Select the worker (e.g. Worker1)
  - Configure --> Under Launch method: change the Host to the Private IP of the first Jenkins worker.
  - Return to the worker (Worker1) and press 'Trust SSH Host Key'.
  - Relaunch the agent and it should be online.
- Repeat this process for the second worker (Worker2).
- Jenkins should now be ready to launch builds!

```sh
./initialize.sh <ip_of_ec2_instance>
```
---
## 2.2 Terraform AWS Infrastructure and Kubernetes Cluster Setup
### Jenkins View  `1. Initialise`
***

A job in Jenkins uses a pipeline script to create the infrastructure. The order in which it builds is as follows:

1. Git clones repo
2. Builds S3 Bucket
3. Builds Kubernetes Cluster
4. Builds RDS Database
5. Builds Loadbalancer
6. Waits prior to setting up K8
7. Provisions K8 nodes with ssh key
8. Provisions k8 cluster
9. Destroys K8 controller
10. Builds K8 Dashboard
11. Sends notification to Slack

![Building Infrastructure Jenkins job](./images/BuildInfra.png)

- This build involves using:
  -  **Terraform** to build the infrastructure
  -  **Shell** scripts to provision machines
  -  **Ansible** to set up Kubernetes cluster and send Slack notifications


### Job A. `launch_aws_infrastructure_and_k8`
***
- This job builds and provisions the AWS infrastructure (Load balancer, RDS, S3 bucket) and the Kubernetes Cluster.
- It is a pipeline build meaning that each previosu stage must complete successfully before the next one can proceed.
- The build worker and stages are identified in the pipeline script as follows:
```groovy
pipeline {
    agent {label 'Worker1'}
    stages {
```
#### Stage 1. Cloning Git

```groovy
          stage('Cloning Git') {
            steps {
                cleanWs()
                checkout([$class: 'GitSCM', branches: [[name: '*/prod']], extensions: [], userRemoteConfigs: [[credentialsId: '82d7f4ca-fd71-406c-adf8-92a36ba0f9f2', url: 'https://bitbucket.org/Tanya-Moller/assessment4/']]])       
            }
        }
```
- This clones the production `prod` branch of the repo using the credentials already set up inside Jenkins.

#### Stage 2. Building S3 Bucket

```groovy
        stage('Building S3 Bucket') {
          steps{
            sh '''
            cd terraform
            yes | terraform init
            yes | terraform apply -auto-approve -target=module.s3
            cd ..
            '''
            }
          }
```
- This runs the Terraform build for the module `s3`
- The terraform file can be found at `terraform/modules/s3/s3.tf` in the repo.
- It builds an S3 bucket with `versioning` set as `enabled = true` so that bucket itemss can be restored from previous versions.
- It is set as `acl = 'private` to make it private
- It then creates an item within the bucket called `pc_config.txt` which contains the original DB username, password, and endpoint.
- A JSON encoded S3 bucket policy is then created. This allows all actions within S3 such as getting and pushing objects from the specified IPs. The JSON policy code is as follows:

```json
{
        Sid       = "IPAllow"
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:*"
        Resource = [ aws_s3_bucket.default.arn,
          "${aws_s3_bucket.default.arn}/*"]
        Condition = {
          IpAddress = {
            "aws:SourceIp" = [<LIST_OF_IPs_TO_ALLOW_ACCESS_TO>]
          }
        }
}
```

#### Stage 3. Building RDS Database

```groovy
        stage('Building RDS Database') {
          steps{
            sh '''
            cd terraform
            yes | terraform init
            terraform apply -auto-approve -target=module.db
            cd ..
            '''
           }
          }
```
- This runs the Terraform build for the module `db`
- The terraform file can be found at `terraform/modules/db/db.tf` in the repo.
- This job builds the RDS security group, subnet group, Primary and Replica RDS databases.
- The RDS has certain features such as `multi_az = true` for multiple AZs and `backup_window = "21:00-22:00"` for automated snapshots.
- A wordpress RDS is also created as well as Route 53 records for ease of use of the RDS endpoints.

#### Stage 4. Building Kubernetes Cluster

```groovy
        stage('Building Kubernetes Cluster') {
          steps{
            sh '''
            cd terraform
            yes | terraform init
            terraform apply -auto-approve -target=module.kubernetes
            cd ..
            '''
            }
           }
```
- This runs the Terraform build for the module `kubernetes`
- The terraform file can be found at `terraform/modules/kubernetes/kubernetes.tf` in the repo.
- It creates a K8 security group.
- It creates a **Kubernetes controller** which is provisioning using the `provision.tpl` file in the same directory.
  - The necessary variables are passed in to the `user_data`.
  - The provisioning script installs Kubespray for the setup of the Kubernetes cluster and populates the DB so it is ready for connection.
  - The details of the code along with the notes can be found in the file.
- 2 master and 3 node instances are created with the necessary specifications and user data as visible in the code.
- Local files are then created in the machine to allow the private IPs of these instances to be used by Jenkins in future builds (SSH and SCP for example).

#### Stage 5. Building Loadbalancer

```groovy
        stage('Building Loadbalancer') {
          steps{
            sh '''
            cd terraform
            yes | terraform init
            terraform apply -auto-approve -target=module.loadbalancer
            cd ..
            '''
           }
          }
```
- This runs the Terraform build for the module `loadbalancer`
- The terraform file can be found at `terraform/modules/loadbalancer/loadbalancer.tf` in the repo. 
- This creates a loadbalancer security group, loadbalancer, target groups, listeners and DNS records.
- A target group is created with `protocol = "HTTP"` and is attached to the master nodes to allow K8 to communicate with it.
  - This is for PetClinic, Wordpress, and the static NGINX sites.
  - It also has a listener on port 80.
- Another target group with `protocol = "HTTPS"` is created and attached to the K8 master nodes.
  - This is for the Kubernetes Dashboard
  - It has a listener on port 443 with an SSL policy and pre-made SSL/TLS certificate attached which allows access on this port.
- Route 53 records are creates for custom DNS names for each server.
  - This is also where new DNS names will be added and applied when multiple static webservers or Wordpress servers etc are created.

#### Stage 6. Sleep

```groovy
        stage ("Wait prior to setting up K8") {
            steps{
                sh '''
                echo 'Waiting 3 minutes for deployment to complete prior to provisioning K8'
                sleep 180
                '''
                }
        }
```
- This step waits for 3 minutes to allow the K8 instances to initialise before setting up the K8 cluster

#### Stage 7. Provisioning K8 Nodes

```groovy
        stage('Provisioning K8 Nodes (SSH)') {
          steps{
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh '''
            master0_ip=`cat /home/ec2-user/master0_ip`
            master1_ip=`cat /home/ec2-user/master1_ip`
            node0_ip=`cat /home/ec2-user/node0_ip`
            node1_ip=`cat /home/ec2-user/node1_ip`
            node2_ip=`cat /home/ec2-user/node2_ip`
    
            ./pubkey.sh $master0_ip
            ./pubkey.sh $master1_ip
            ./pubkey.sh $node0_ip
            ./pubkey.sh $node1_ip
            ./pubkey.sh $node2_ip
            '''
            }
           }
          }
```
- This job uses SSH credentials to SSH into the instances.
- The IPs that were put into local files using Terraform are now sourced as variables to easily SSH into the machines and make the necessary changes.
- The file `./pubkey.sh` is run on all the K8 machines.
  - This files pastes the public SSH key from the K8 controller to the nodes into `~/.ssh/authorized_keys` to allow the controller to set up the K8 cluster using Kubespray in the next step.
  - It will replace the key if it has changed or leave it the same if it matches.
  - The key is also removed from the machine at the end so it is not accessible locally.

#### Stage 8. Provisioning K8 Cluster

```groovy
        stage('Provisioning K8 Cluster') {
          steps{
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh '''
            controller_ip=`cat /home/ec2-user/controller_ip`
            
            ssh -o StrictHostKeyChecking=no ec2-user@$controller_ip '
            cd /home/ec2-user/kubespray
            ansible-playbook --ssh-common-args "-o StrictHostKeyChecking=no" -i /home/ec2-user/kubespray/inventory/mycluster/hosts.yaml --become --become-user=root /home/ec2-user/kubespray/cluster.yml
            '
            '''
            }
           }
          }
```
- This step uses SSH credentials to SSH into the instances.
- The IP that was put into a local file using Terraform is now sourced as a variable to easily SSH into the K8 controller and set up the K8 cluster.
- This runs the ansible playbook which provisions the entire cluster.

#### Stage 9. Destroy K8 Controller

```groovy
        stage('Destroy K8 Controller') {
          steps{
            sh '''
            cd terraform
            yes | terraform init
            terraform destroy -auto-approve -target=module.kubernetes.aws_instance.kubernetes_controller
            cd ..
            '''
           }
          }
```
- This job destroys the Kubernetes controller by specifically targetting the resource of the K8 controller instance as it is no longer needed.

#### Stage 10. Building K8 Dashboard

```groovy
        stage('Building K8 Dashboard') {
          steps{
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh '''
            master1_ip=`cat /home/ec2-user/master1_ip`

            scp k8-dashboard/haproxy-ingress.yml ec2-user@$master1_ip:/home/ec2-user/haproxy-ingress.yml
            scp k8-dashboard/kubernetes-dashboard-deployment.yml ec2-user@$master1_ip:/home/ec2-user/kubernetes-dashboard-deployment.yml

            ./k8-dashboard/k8_dashboard.sh $master1_ip
            '''
            }
           }
          }
```
- This step uses SSH credentials to SSH into the instances.
- The IP that was put into a local file using Terraform is now sourced as a variable to easily SSH into the K8 master node and set up the K8 Dashboard.
- The files `haproxy-ingrss.yml` and `kubernetes-dashboard-deployment.yml` are secure copied onto the machine.
  - These files can be found in `k8-dashboard/` in the repo.
- The file `k8-dashboard/k8_dashboard.sh` is then run on the master node to set up the dashboard.
  - The code has the necessary notation for the set up of the dashboard.
  - Alternatively `README_Manual_K8_Dash_Setup.md` explains how to do this manually.

**To access the dahsboard**


A. Download certificate
- Ensure the AWS profile is exported `export AWS_PROFILE=Academy`
- Ensure the S3 bucket has a policy attached to allow access from home IP.
- Download the certificate from S3 bucket
```sh
aws s3 cp s3://ratstar-bucket/kubernetes-dashboard-public.crt ./
```
- Put the certificate into Keychain Access (Mac) on local machine.
- Open (double-click) the certificate and select 'Always Trust' under 'Trust'.


B. Access the Dashboard
- Go to the DNS of your loadbalancer
- Ensure you are on https:// and that your loadbalancer has security group rule for port 443
- Select 'proceed even if unsafe' if necessary
- Paste the token into the UI - This is created in the Jenkins build `k8_dashboard_token_generator` (read the console output for the token)
- Log in :)

#### Stage 11. Slack notification

```groovy
        stage('Slack notification') {
          steps{
            sh '''
            sed -i "s/msg: 'Your test is complete :fiestaparrot:'/msg: 'AWS infrastructure and Kubernetes Cluster build complete :rambanultra:'/" ansible-slack/slack.yml
            ansible-playbook ansible-slack/slack.yml
            '''
           }
          }
```
- This stage uses the file `ansible-slack/slack.yml` in the repo.
- This yaml files sends a notification to the academy api testing channel when applied using Ansible.
- This step replaces the message to reflect the job that has completed and runs the playbook.
***
# 3. Docker Registry 


### Docker Images

All the versions of PetClinic's images are available on DockerHub. Anyone with the password and username will be able to access dockerhub to view the private repository.

The link to the repository:
```sh
https://hub.docker.com/repository/docker/ratstardocker/petclinic

Username: ratstardocker
Password: automation logic
```

A jenkins job named docker_build will pull in the latest code from the dev_envriroment and build a docker-image from the updated code. If the build passes tests then the docker image is pushed to this private repository, and it will be tagged with the jenkins-build-number. Should the developers want to pull a previous version of the image, they can change the variable $VERSION in the docker_build job, to the tag number which they want to use.

E.g To pull in version 120 
```sh
Present code: VERSION = "${env.BUILD_ID}"
Modify to : VERSION = "${120}"
```
Because this is a private repository you will only be able to pull in the image with the correct kubernetes seceret. The command used to configure the docker registry seceret is :
```sh
kubectl create secret docker-registry myregistrykey3 --docker-username=ratstardocker --docker-password=automationlogic --docker-email=ayesha@automationlogic.com -n petclinic
```
---
# 4. Deployment 

## 4.1 PetClinic
***

The Kubernetes Deployment of the PetClinic application is split into three parts. It's `deployment.yml`, `service.yml` and `ingress.yml`. These separate components are defined in a `yml` file, and any changes you want to make to the Kubernetes deployment must be made in these files.

### Deployment 
```yml
apiVersion: apps/v1
## Defining the "kind" of file to specify Deployment
kind: Deployment
metadata:
  namespace: petclinic
  name: petclinic
  labels:
    app: petclinic
spec:
  ## Set the number of desired pods 
  replicas: 3
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
      maxSurge: 2
  selector:
    matchLabels:
      app: petclinic
  template:
    metadata:
      labels:
        app: petclinic  
    spec:
      imagePullSecrets:
      ## Use of Kubernetes Secrets to store PetClinic Password is being called here to allow access to our Private Docker Registry
      - name: myregistrykey3
      containers:
      - name: petclinic
      ## Name of Image to pull from our Private Docker Registry. Our Jenkins pipeline updates this build number "40" with the latest one. Build number 40 acts as a placeholder
        image: ratstardocker/petclinic:40
        imagePullPolicy: Always
        ports:
        - containerPort: 8080
        env:
        - name: PCDBHOST
        ## These values "PLACEHOLDER" get replaced in the Pipeline, this means our passwords and other sensitive information is not visible in our repo 
        ## The value for PCDBHOST is important to configure if you want to connect with another DB, this change must be made in the pipeline and is explained below. You must ensure to change it in the `ExternalName` service too.
          value: PLACEHOLDER
        - name: PCDBUSER
          value: PLACEHOLDER
        - name: PCDBPASS
          value: PLACEHOLDER
        ## This defines the conditions for the Readiness Probe.
        ## This Readiness Probe allows for smooth transitions between Pod Deployment. If a new pod is deployed, the probe waits for a healthy status check from the new pod and only then will it reroute traffic to the new pods and delete the old ones.
        readinessProbe:
          initialDelaySeconds: 5
          periodSeconds: 15
          timeoutSeconds: 2
          successThreshold: 2
          failureThreshold: 2
          httpGet:
            path: /
            port: 8080
        ## These are are the resources and metrics that the readiness probe reads and makes it 
        resources:
          limits:
            cpu: 500m
          requests:
            cpu: "200m"
            memory: "64Mi"
```
- This job destroys the Kubernetes controller by specifically targetting the resource of the K8 controller instance as it is no longer needed.

#### **Stage 10. Building K8 Dashboard**

```groovy
        stage('Building K8 Dashboard') {
          steps{
            sshagent(credentials : ['82d7f4ca-fd71-406c-adf8-92a36ba0f9f2']) {
            sh '''
            master1_ip=`cat /home/ec2-user/master1_ip`

#### PetClinic Horizontal Pod Autoscaling 

Horizontal Pod Autoscaling will allow Kubernetes to accomodate an increase (or decrease) in traffic by creating (or destroying) pods according to predefined metrics. In our case this will happen according to CPU utilization and network traffic. 

```bash
kubectl autoscale deployment petclinic -n petclinic --cpu-percent=50 --min=3 --max=10
```

To activate autoscaling after petclinic deployment

```bash
kubectl autoscale deployment petclinic -n petclinic --cpu-percent=50 --min=3 --max=10
```
- Put the certificate into Keychain Access (Mac) on local machine.
- Open (double-click) the certificate and select 'Always Trust' under 'Trust'.


B. Access the Dashboard
- Go to the DNS of your loadbalancer
- Ensure you are on https:// and that your loadbalancer has security group rule for port 443
- Select 'proceed even if unsafe' if necessary
- Paste the token into the UI - This is created in the Jenkins build `k8_dashboard_token_generator` (read the console output for the token)
- Log in :)

#### **Stage 11. Slack notification**

```groovy
        stage('Slack notification') {
          steps{
            sh '''
            sed -i "s/msg: 'Your test is complete :fiestaparrot:'/msg: 'AWS infrastructure and Kubernetes Cluster build complete :rambanultra:'/" ansible-slack/slack.yml
            ansible-playbook ansible-slack/slack.yml
            '''
           }
          }
```
- This stage uses the file `ansible-slack/slack.yml` in the repo.
- This yaml files sends a notification to the academy api testing channel when applied using Ansible.
- This step replaces the message to reflect the job that has completed and runs the playbook.
***
# <a name='Docker Registry'></a>3. Docker Registry 


### Docker Images

  - This command alters our deployment and ensures at minimum there are 3 pods with PetClinic up and running. In the case of high traffic (or any other criteriea met for autoscaling), Kubernetes will launch a maximum of 10 pods to accomodate an increase in traffic. 
  - This command also defines a rule to maintain an average of 50% CPU utilzation across all Pods.

Command to check autoscaling
```bash
kubectl get hpa -n petclinic
```
##### Metric Server

In order for the metrics to be read, a metric server must be deployed. The configuration is found in the file `petclininc/K8/metric_server.yml`

This file is ran automatically in the pipeline and does not need any configuration.

### Service

In order to connect all the pods so that they're not separate instances, a service is used to create a single IP and DNS name for a set of Pods. This also solves the problem of ephemerality as individual Pods (and the unique IP associated to them) by nature get created and destroyed continously to ensure they match the state we've defined. The service we create ensure all Pods that come and go can be accessed through a single, permanant way.

There are many types of services, for this project we need the use of the `ClusterIP` and `ExternalName` service.

```yml

apiVersion: v1
kind: Service
metadata:
  namespace: petclinic
  labels:
    app: petclinic
  name: petclinic-clip-svc
spec:
  ports:
    ## As PetClinic runs in the Pod on Port 8080, we map it to Port 80 via this Cluster IP service
  - port: 80
    protocol: TCP
    targetPort: 8080
  selector:
    app: petclinic
  type: ClusterIP

--- 

apiVersion: v1
kind: Service
metadata:
  namespace: petclinic
  name: petclinic-db-svc
  labels:
      app: petclinic
spec:
  type: ExternalName
  externalName: ratstar-db.academy.labs.automationlogic.com 
```
In order to make a connection to our external RDS DB, we use an `ExternalName` service to do so. If you need to change the connection of the database the PetClinic application uses, you need to change the value for `externalName` here and the value for the key `PCDBHOST` in the `deployment` stage.

### Ingress

In order to be able to externally access the ClusterIP service an Ingress rule is required. This is specified by the following.

```yml

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    namespace: petclinic
    name: petclinic-ingress
spec:
    rules:
    ## The PetClinic Cluster IP is only accessible through this following DNS.
    - host: "ratstar-petclinic.academy.labs.automationlogic.com"
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: petclinic-clip-svc
              port:
                number: 80
```


In the Terraform creation of the infrastructure we create an external Loadbalancer which listens to a `NodePort` that we have defined in the next section. As we mapped the PetClinic on the Pod's Port 8080 to Port 80 in the Cluster IP service, we tell this Ingress to connect to that Port 80 in the Cluster IP service.

### Ingress Controller 

In order to manage the connection of the Loadbalancer and the ingress, we need to configure a Ingress controller. This is done in the file `petclinic/K8/haproxy-service.yml`. This file is run automatically through the Jenkins pipeline, however the important section is the following.

```yml
apiVersion: v1
kind: Service
metadata:
  labels:
    run: haproxy-ingress
  name: haproxy-ingress
  namespace: haproxy-controller
spec:
  selector:
    run: haproxy-ingress
  type: NodePort
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
    ## We pin the NodePort to 31247, this is what our external Loadbalancer listens to in order to connect to the PetClinic Ingress.
    nodePort: 31247
  - name: https
    port: 443
    protocol: TCP
    targetPort: 443
  - name: stat
    port: 1024
    protocol: TCP
    targetPort: 1024
```
We use HAProxy Ingress controller which listens to Ingresses across all namespaces. This particular one will map any Ingress that runs on Port 80 to nodeport 31247. A node port is simply a service which allows connection to a cluster from outside that cluster. 

This is the same Ingress controller which allows for the connection for the Static Website and WordPress apps too. As explained later, it is the specific application's Ingress rule's `host:` is what allows differentiation between the different applications.

Finally, the three parts (`deployment`, `service`, and `ingress`) can be written in one file for ease and has been done so in the file `petclinic/K8/k8-petclinic.yml`.


---
## 4.2 WordPress
### Description of Application Set Up

WordPress is a simple application which can be used to write blogs to share. The data of WordPress is stored in a separate RDS that is created through the Terraform build.

The `service`, `deployment`, and `ingress` all are similar to the ones for the PetClinic application shown above. 

### Launching New Blogs

In order to launch a new blog, follow these steps:

1. Click `Build with Parameters` in the Jenkins job `wordpress_new_blog`

![TAG](./images/new_blog.png)
<!-- <img src="./images/new_blog.png" width="500"> -->

1. Choose the tag you would like your blog to have.
   - Please remember, this must be unique.
2. You will recieve a notification Slack with a new DNS which has your new WordPress blog to set up.

For example, if you had chosen the tag "george", you would recieve the DNS: `http://ratstar-wordpress-george.academy.labs.automationlogic.com/` 


#### Detail of Jenkins Job

This job is run off the script found at `wordpress/Jenkins_Scripts/deploy_wp_new_user.sh`

This script initally checks if the tag that the user defines is in use by checking if there is a namespace already created with that tag. All extra blogs are created in their own unique namespace with the formatting `wordpress_george` (if the tag was to be "george"). If this namespace already exists then the build is marked as a failure and the user is asked to pick another tag. 

If the tag is unique, it creates a copy of the file found in `wordpress/K8_extra_users/k8-wordpress-rds-extra.yml` which is essentially a template used to create the new K8 resources. The script has the string "REPLACE" at the end of certain lines in order for a `sed` command to replace that word with the chosen tag. New K8 resources are created for the new WordPress blog, including namespaces, pods, deployment, etc. 

Additionally, a connection is made with the WordPress RDS to create a new table to accomodate the new user's blog. After this script has ran, the Jenkin's build then adds a new Route 53 DNS name and applies it through Terraform. Importantly, it checks the exit codes of the script before and only performs this if the script before executes succesfully.

---
## 4.3 Static Website
To deploy a static website with a custom DNS through Kubernetes and terraform follow these steps.

1. Locate the jenkins buld job

The Jenkins job for this is called "deploy_static_website" on the prod branch, and it is a parametrised build. 

2. Configure the build
   
You simply need to configure the job by adding a tag for the name of the static website you wish to launch.

![TAG](./images/ta_image.png)

3. Build job

Hit build and job with will deploy the new static website in the namespace called the value 'TAG'. And the job will also create a new DNS through Terraform called

```ratstar-web-"TAG".academy.labs.automationlogic.com```

4. Slack notification
   
Once the build has finished this sends a notification to slack with the DNS attached.
### Jenkins job
The jenkins job does the following:
1. Creates new DNS
2. Creates a directory for the new static website in the master node
3. Populates this directory with the static website repo
4. Edits the files to contain the correct new namespace
5. Runs the `static_web_deploy.sh` file to deploy the new static website on kubernetes.

### Static web deploy file

1. sets up the config file and correct path
```
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
```
2. Creates the namespace
``` 
Kubectl create namespace <name>
```
3. Creates the configmap
```
kubectl create configmap <configmap name> --from-file=<file/dir to create from> -n <namespace>
```
4. Runs all the files to deploy the static website
```
kubectl apply -f <dir>
```
5. Replaces the configmap
```
kubectl create configmap <configmap name> --from-file=<file/dir> -oyaml --dry-run | kubectl replace -f - -n <namespace>
```
This last step is only needed if you wish to edit the html/configmap files while the pods are up and running. Only needed if you wish to make live changes to that static website. 
***
-



---
# 5. Management
**Jenkins View  `3. Manage`**
- These management jobs allow attributes such as password or DB connections to be updated or changed based on the requirements of the user.

## 5.1 Generate Token for Kubernetes Dashboard
***

### Job `k8_dashboard_token_generator`
- This job builds after the AWS infrastucture and Kubernetes Cluster to provide the token to log in to Kubernetes dashboard.
- It runs the script `k8-dashboard/k8_token.sh` which generates a unique token for the login to Kubernetes dashboard.

```sh
#!/bin/bash

##Set the K8 master 2 IP as a variable
master1_ip=`cat /home/ec2-user/master1_ip`

##Copy the shell scipt to generate a token to the master node
scp -o StrictHostKeyChecking=no /home/ec2-user/workspace/k8_dashboard_token_generator/k8-dashboard/k8_token.sh ec2-user@$master1_ip:/home/ec2-user/k8_token.sh

##Print the generated token generated running the shell script
echo "THIS IS YOUR NEW TOKEN:"
ssh -o StrictHostKeyChecking=no ec2-user@$master1_ip '
chmod +x /home/ec2-user/k8_token.sh
/home/ec2-user/k8_token.sh
'
```

## 5.2 Change PetClinic RDS Password
***

### Job `password_change`
- This job changes the petclinic RDS password.
- The job uses an input parameter for the user to create a new password easily.
- This parameter is then used to update the terraform files for the db and db_restore in the modules `db` and `db_restore` and applies the changes to the primary DB.

```sh
#!/bin/bash

##Copy the DB username, password and host data file from S3 Bucket
aws s3 cp s3://ratstar-bucket/pc_config.txt ./


cd terraform

##Change the pasword in the db and db_restore Terraform files using the newly input parameter
sed -i "s,password = .*,password = \"$Passphrase\",g" ./modules/db/db.tf

sed -i "s,password = .*,password = \"$Passphrase\",g" ./modules/db_restore/db_restore.tf

##Apply the changes using Terraform to update the DB
yes | terraform init
terraform apply -auto-approve -target=module.db.aws_db_instance.default
cd ..

##Update the config file with the new password
cat >pc_config.txt <<_END_ 
Username: "petclinic"
Endpoint: \"ratstar-db.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com\"
Password: $Passphrase
_END_

##Check the changes have been applied
cat pc_config.txt

##Replace the config file with the updated version for future use.
aws s3 cp pc_config.txt s3://ratstar-bucket/pc_config.txt 
```
- It also changes the petclinic config file `pc_config.txt` and uploads it to the private S3 bucket for use when configuring Petclinic.
- Hence, this job triggers the job `k8_deploy_petclinic` afterwards to update Petclinic with the new password.

## 5.3 Change PetClinic RDS Connection
***

### Job `petclinic_update_db_connection`
- This job changes the DB password, username, & host in the PetClinic config.
- It would be used to migrate the servers to a new DB (e.g. in the event of a restore).
- It is important to ensure it matches the DB you are connecting PetClinic to otherwise it will not connect correctly.
- The job uses input parameters for the user to input a new password, username, and endpoint for a new database easily.

```sh
#!/bin/bash

## Setting variables for the image version and K8 master IP
VERSION=`cat /home/ec2-user/workspace/docker_build/tag2.txt`
master0_ip=`cat /home/ec2-user/master0_ip`

## Replacing the necessary variables in the K8 files
sed -i "s,petclinic.db.host: PLACEHOLDER,petclinic.db.host: $PC_DB_ENDPOINT," petclinic/K8/k8-petclinic.yml
sed -i "s,petclinic.db.user: PLACEHOLDER,petclinic.db.user: $PC_USER," petclinic/K8/k8-petclinic.yml
sed -i "s,petclinic.db.password: PLACEHOLDER,petclinic.db.password: $PC_PW," petclinic/K8/k8-petclinic.yml

sed -i "s,image: ratstardocker/petclinic:40,image: ratstardocker/petclinic:$VERSION," petclinic/K8/k8-petclinic.yml
##Ensure that the changes have occurred
cat petclinic/K8/k8-petclinic.yml

##Copy the necessary files into the K8 master and deploy Petclinic connected to the new DB
ssh -o StrictHostKeyChecking=no ec2-user@$master0_ip 'rm -rf /home/ec2-user/petclinic;mkdir /home/ec2-user/petclinic'
scp -o StrictHostKeyChecking=no -r petclinic/* ec2-user@$master0_ip:/home/ec2-user/petclinic
ssh -o StrictHostKeyChecking=no ec2-user@$master0_ip '/home/ec2-user/petclinic/Jenkins_Scripts/deploy_pc_k8.sh'
```
- Using shell scipt (sed and scp), it updates the necessary information in the K8 deployment scripts to reflect the changes and deploys the new Petclinic image connected to the correct DB.

## 5.4 Restore Database
***

### Job `restore_database`
- This job restores the db to a new instance using a specified snapshot - This can be found using the AWS UI.
- The job uses input parameters for the user to input a new tag and snapshot ID for a database restore easily.

```sh
#!/bin/bash

##Create a new name for the restore DB based on the TAG and AWS_SNAPSHOT parameters
sudo sed -i "s/restore/restore$TAG/" terraform/modules/db_restore/db_restore.tf

sudo sed -i "s/rdssnap/$AWS_SNAPSHOT/" terraform/modules/db_restore/db_restore.tf

##Run Terraform to apply the changes
cd terraform

yes | terraform init
terraform apply -auto-approve -target=module.db_restore
```
- It inputs the parameters into the Terraform files for the new DB. 
- This also ensures that no problems are caused in the tfstate files, given that the tag has not been used before.
- It then runs the changes, building a newly restored RDS.

# 6. Development Environment

The PetClinic Dev Environment allows a developer to make changes to the source code of PetClinic and see the results of those changes in the form of a ready to see website through Kubernetes. This is due to the ability to launch custom PetClinic Dev Environments similar to launching new WordPress blogs and new static websites.

## Using the Dev Environment

The developer must first checkout to the `dev` branch. To do this
```bash
git checkout dev
```

In there the developer will see the source code folder for petclinic at `petclinic_dev/pcsource`. Any changes can be made there and its result can be seen in the form of a website by running our Jenkins build. This is explained below.

## Launching A New PetClinic Dev Env

The code which launches a new PetClinic Dev Env works the same as the ones which launch a new WordPress Blog and static website. 

In order to begin:
1. Ensure you've made changes to the code in the git branch `dev`
2. Launch the Jenkins Job `k8_deploy_petclinic_dev_new` and specify the name tag of the Dev Env you'd like to launch.
   - Please note this must be a unique tag
3. You will then recieve a DNS which has your PetClinic changes deployed on a website. 

## To Launch These Dev Changes To Production
1. Ensure you're happy with the changes that you can see on your website.
2. Without changing the code, simply run the job `docker_build`. 
   - This build will create your build and push it to Docker repo and tag it as the latest production image.
3. Upon successful completion, the following build `k8_deploy_petclinic` will be triggered and the new changes will be pushed into the production environment.
