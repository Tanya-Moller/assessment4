#!/bin/bash

if (( $#  > 0 ))
then
    hostname=$1
else
    echo "WTF: you must supply a hostname or IP address" 1>&2 
    exit 1 
fi 

##SSH into the machine
ssh -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/ratstar_key ec2-user@$hostname '

mkdir kubernetes-dashboard
cd kubernetes-dashboard

##Set up config
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf

##Copy in the dashboard and ingress yaml
mv /home/ec2-user/kubernetes-dashboard-deployment.yml ./kubernetes-dashboard-deployment.yml
mv /home/ec2-user/haproxy-ingress.yml ./haproxy-ingress.yml

##Apply the files
kubectl apply -f kubernetes-dashboard-deployment.yml
kubectl apply -f haproxy-ingress.yml

##Create an admin account and apply it
echo "---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: jmutai-admin
  namespace: kube-system">admin-sa.yml

kubectl apply -f admin-sa.yml

##Create cluster role binding and apply it
echo "---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: jmutai-admin 
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: jmutai-admin 
    namespace: kube-system">admin-rbac.yml

kubectl apply -f admin-rbac.yml
'
