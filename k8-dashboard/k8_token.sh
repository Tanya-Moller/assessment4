#!/bin/bash


##Update the config
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
SA_NAME="jmutai-admin"

##Get and print a new token
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep ${SA_NAME} | awk '{print $1}') | grep token | awk '{print $2}' | tail -n +3