# Kubernetes Dashboard - Manual Setup

### Deploy Kubernetes dashboard
- https://computingforgeeks.com/how-to-install-kubernetes-dashboard-with-nodeport/
- Inside the master node - run this command first `sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf`
```sh
wget https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml
mv recommended.yaml kubernetes-dashboard-deployment.yml
```

- Edit the `kubernetes-dashboard-deployment.yml` file with the following:

```yml
---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 443
      targetPort: 8443
      nodePort: 31106 #Set this
  selector:
    k8s-app: kubernetes-dashboard
  type: NodePort #This is the line that needs to be added
```

### Apply the changes
```sh
kubectl apply -f kubernetes-dashboard-deployment.yml
kubectl get service -n kubernetes-dashboard #Check for NodePort 
```
- Note the port `443:<PORT>/TCP`

### Create an ingress rule
- Create a file `haproxy-ingress.yml` and input the following using a valid DNS name
  - The DNS name will be of the loadbalancer that communicates with the cluster
  
```yml
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: kubernetes-dashboard-ingress
  namespace: kubernetes-dashboard
  annotations:
    haproxy.org/server-ssl: "true"
spec:
    tls:
    - hosts:
      - "<DNS_NAME>" #Replace this
      secretName: kubernetes-dashboard-secret
    rules:
    - host: "<DNS_NAME>" #Replace this
    - http:
	paths:
  - path: /
          pathType: Prefix
          backend:
            service:
              name: kubernetes-dashboard
              port:
                number: 443
```
- Run the file

```sh
kubectl apply -f haproxy-ingress.yml
kubectl get ingress -n kubernetes-dashboard
```

### Generate an AWS key and certificate
- https://aws.amazon.com/premiumsupport/knowledge-center/eks-kubernetes-dashboard-custom-path/
- Ensure the instance has IAM role for AWS Certificates

```sh
openssl genrsa 2048 > kube-dash-private.key
openssl req -new -x509 -nodes -sha1 -days 3650 -extensions v3_ca -key kube-dash-private.key > kube-dash-public.crt
```
- This will request input - The important part is to list the `Common Name` as your DNS name attached to your load balancer.
- It looks like this:

```sh
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:   
Common Name (eg, your name or your servers hostname) []: <DNS_NAME>        #==>This is important
Email Address []:
```
- Upload the certificate to AWS
```sh
aws acm import-certificate --certificate file://kube-dash-public.crt --private-key file://kube-dash-private.key --region us-east-1 #Replace region
```

### Create Target Group & Listener on Load Balancer

- Create a target group which targets the master nodes on the NodePort IP of the kubernetes-dashboard service we saw earlier.
- Create a Listener on port 443 with the generated certificate and target group attached

### Create an admin user
- https://computingforgeeks.com/create-admin-user-to-access-kubernetes-dashboard/
- Create a file `admin-sa.yml` and input the following:
```yml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: jmutai-admin #Replace this with your username
  namespace: kube-system
```
- Apply the file
```sh
kubectl apply -f admin-sa.yml
```
- Create a file for cluster role binding `admin-rbac.yml` and input the following:
```yml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: jmutai-admin #Replace this
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: jmutai-admin #Replace this
    namespace: kube-system
```
- Apply the file
```sh
kubectl apply -f admin-rbac.yml
```


### Generate a Token for the log in
- Set a variable to store the name of the service account
```sh
SA_NAME="<username>"

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep ${SA_NAME} | awk '{print $1}')
```
- Copy the token from the output which looks like this:
```sh
Type:  kubernetes.io/service-account-token

Data
====
token:      #<TOKEN WILL BE HERE>
ca.crt:     1025 bytes
namespace:  11 bytes
```

### Download certificate
- Download the certificate from S3 bucket
- Ensure the AWS profile is exported `export AWS_PROFILE=Academy`
- Ensure the S3 bucket has a policy attached to allow access from home IP.

```sh
aws s3 cp s3://ratstar-bucket/kubernetes-dashboard-public.crt ./
```
- Put the certificate into Keychain Access (Mac) on local machine.
- Open (double-click) the certificate and select 'Always Trust' under 'Trust'.

### Access the Dashboard
- Go to the DNS of your loadbalancer
- Ensure you are on https:// and that your loadbalancer has security group rule for port 443
- Select 'proceed even if unsafe' if necessary
- Paste the token into the UI
- Log in :)