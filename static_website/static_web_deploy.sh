#!/bin/bash
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
sudo mkdir -p /usr/share/nginx/html
cd /home/ec2-user/static_website/k8-sw
# create namespace
kubectl create namespace nginx
# creating the config map index.html
kubectl create configmap index --from-file=html -n nginx

# apply the files
cd /home/ec2-user/static_website/

kubectl apply -f k8-sw

# if you want to update use - and this will update your config map - so you can 
cd /home/ec2-user/static_website/k8-sw
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
kubectl create configmap index --from-file=html -oyaml --dry-run | kubectl replace -f - -n nginx
