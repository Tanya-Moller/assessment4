#!/bin/bash

sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf


if kubectl get namespace | grep $NEW_DEV
then
    echo "$NEW_DEV already exists"
    exit 1 
else

    cd ~/petclinic_dev

    aws s3 cp s3://ratstar-bucket/pc_config.txt pc_config.txt
    PC_PW=`cat pc_config.txt | grep Password: | awk '{split($0,a,": "); print a[2]}'`

    cp K8/k8-petclinic-dev-extra.yml K8/k8-petclinic-dev-$NEW_DEV.yml 
    cp K8/k8-petclinic-dev-namespace.yml K8/k8-petclinic-dev-$NEW_DEV-namespace.yml

    sed -i "s,REPLACE,$NEW_DEV," K8/k8-petclinic-dev-$NEW_DEV.yml 
    sed -i "s,REPLACE,$NEW_DEV," K8/k8-petclinic-dev-$NEW_DEV-namespace.yml
    sed -i "s,PASSWORDHERE,$PC_PW," K8/k8-petclinic-dev-$NEW_DEV.yml 

    cat K8/k8-petclinic-dev-$NEW_DEV.yml 

    cd /home/ec2-user/petclinic_dev/Docker

    sudo docker build --no-cache -t petclinic_dev .
    sudo docker tag petclinic_dev ratstardocker/petclinic_dev:$NEW_DEV

    cd ..
    kubectl apply -f K8/k8-petclinic-dev-$NEW_DEV-namespace.yml
    kubectl create secret docker-registry myregistrykey3 --docker-username=ratstardocker --docker-password=automationlogic --docker-email=ayesha@automationlogic.com -n petclinic-dev-$NEW_DEV

    sudo docker push ratstardocker/petclinic_dev:$NEW_DEV


    sudo mkdir -p /home/ec2_user/mysql_init_temp
    sudo cp Docker/mysql/* /home/ec2_user/mysql_init_temp
    sudo sed -i "s,petclinic,petclinic_dev_$NEW_DEV,g" /home/ec2_user/mysql_init_temp/1.sql
    cat /home/ec2_user/mysql_init_temp/1.sql

    sudo yum install -y mariadb
    sudo mysql -h ratstar-db.academy.labs.automationlogic.com -u petclinic -p$PC_PW  --execute="source /home/ec2_user/mysql_init_temp/1.sql;"
    sudo mysql -h ratstar-db.academy.labs.automationlogic.com -u petclinic -p$PC_PW  --execute="source /home/ec2_user/mysql_init_temp/2.sql;" --database=petclinic_dev_$NEW_DEV
    sudo mysql -h ratstar-db.academy.labs.automationlogic.com -u petclinic -p$PC_PW  --execute="source /home/ec2_user/mysql_init_temp/3.sql;" --database=petclinic_dev_$NEW_DEV

    sudo rm -rf /home/ec2_user/mysql_init_temp


    kubectl apply -f K8/k8-petclinic-dev-$NEW_DEV.yml
    exit 0
fi



